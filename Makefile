PKGNAME := rockyou2021
PREFIX := /usr
LINK   := 'https://mega.nz/file/q84SlLxK#aazSxjsHP1AokapSIJqlPZeHU4qFl7Ci2jRGeQTvo1Y'

prepare: clean
	megatools dl $(LINK) --path ${PKGNAME}.txt.gz
	gzip -c -d ${PKGNAME}.txt.gz > ${PKGNAME}.txt

install:
	install -dm755 $(PREFIX)/share/wordlists
	install -Dm644 ${PKGNAME}.txt $(PREFIX)/share/wordlists/${PKGNAME}.txt

clean:
	rm ${PKGNAME}.txt
